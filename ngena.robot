*** Settings ***
Library           Selenium2Library
Resource          variables.txt
Library           OperatingSystem
Library           String
Library           Process
Library           ExcelLibrary
Library           robot.api.logger

*** Test Cases ***
TC1_Login_verification
    [Documentation]    This Test Case launches & logs into the portal.
    Login_Form

TC2_customer_Solution_creation
    Customer Solution Form

TC3_Access_availablity_verification
    Access_Availability_Form

*** Keywords ***
Access_Availability_Form
    log    'clicking on the filtered customer solution name'
    sleep    5s
    Click Element    //div[@class=' table table-condensed table table-hover']//a/span[contains(text(),'${customerSolutionName}')]
    ${CustomerSolutionPage}    Get Text    //div[@class='main_heading pull-none clearfix']//span[contains(text(),'Customer Solution')]
    Page Should Contain    ${CustomerSolutionPage}
    log    'Clicking on Quoatation Process Option'
    Click Element    //ul[@class='process_selector']//li//span[contains(text(),'Quotation process')]
    ${QuotationPage}    Get Text    //div[@class='main_heading pull-none clearfix']//span[contains(text(),'Quotation process')]
    Page Should Contain    ${QuotationPage}
    log    'Clicking on Quoatation Item BUDO'
    Sleep    10s
    Click Element    //div[@class='csrt_search-browser']//tr//a[contains(text(), 'BUDO/2017')]
    ${Budgetaryquote}    Get Text    //div[@class='main_heading pull-none clearfix']//span[contains(text(),'Budgetary quote')]
    Page Should Contain    ${Budgetaryquote}
    log    'Clicking on Continue Registration Option'
    sleep    5s
    Click Element    //li//a//span[contains(text(),'Continue registration')]
    log    'Enter the address from the xls file'
    Open Excel    ${CURDIR}\\Address.xls
    log    ${Rowcount}
    : FOR    ${Country}    IN RANGE    1    ${Rowcount}
    \    ${colNum}    Set Variable    0
    \    ${country_name}=    Read Cell Data By Coordinates    Data    0    ${Country}
    \    ${street}=    Read Cell Data By Coordinates    Data    1    ${Country}
    \    ${building}=    Read Cell Data By Coordinates    Data    2    ${Country}
    \    ${building}=    Convert To String    ${building}
    \    ${building}=    Strip String    ${building}    characters=.0
    \    ${apt}=    Read Cell Data By Coordinates    Data    3    ${Country}
    \    ${postal}=    Read Cell Data By Coordinates    Data    4    ${Country}
    \    ${postal}=    Convert To String    ${postal}
    \    ${postal}    Strip String    ${postal}    characters=.0
    \    ${City}=    Read Cell Data By Coordinates    Data    5    ${Country}
    \    ${CoordinatesN}=    Read Cell Data By Coordinates    Data    6    ${Country}
    \    ${CoordinatesN}=    Convert To String    ${CoordinatesN}
    \    ${CoordinatesE}=    Read Cell Data By Coordinates    Data    7    ${Country}
    \    ${CoordinatesE}=    Convert To String    ${CoordinatesE}
    \    ${AreaCode}=    Read Cell Data By Coordinates    Data    8    ${Country}
    \    ${AreaCode}=    Convert To String    ${AreaCode}
    \    ${PhoneNumber}=    Read Cell Data By Coordinates    Data    9    ${Country}
    \    ${PhoneNumber}=    Convert To String    ${PhoneNumber}
    \    sleep    3s
    \    log    'Clicking on Add Address Button'
    \    Click Element    //div[@class='inline-actions dropdown-related']//a[contains(text(),'Add address')]
    \    log    'input Address for availability check'
    \    Click Element    //a[@class='select2-choice']//span[@class='select2-arrow']/b
    \    Input Text    //*[@id='s2id_autogen1_search']    ${country_name}
    \    Press Key    //*[@id='s2id_autogen1_search']    \\09
    \    sleep    10s
    \    Input Text    //div[@class='controlcontent']/parent::div//div/label[contains(text(),'Street')]/following-sibling::div/input    ${street}
    \    sleep    2s
    \    Input Text    //div[@class='controlcontent']/parent::div//div/label[contains(text(),'Building No.')]/following-sibling::div/input    ${building}
    \    sleep    2s
    \    Input Text    //div[@class='controlcontent']/parent::div//div/label[contains(text(),'Apt./Suite')]/following-sibling::div/input    ${apt}
    \    sleep    2s
    \    Input Text    //div[@class='controlcontent']/parent::div//div/label[contains(text(),'Postal code')]/following-sibling::div/input    ${postal}
    \    sleep    2s
    \    Input Text    //div[@class='controlcontent']/parent::div//div/label[contains(text(),'City')]/following-sibling::div/input    ${City}
    \    sleep    2s
    \    Input Text    //div[@class='controlcontent']/parent::div//div/label[contains(text(),'Coordinates')]/following-sibling::div/input[1]    ${CoordinatesN}
    \    sleep    2s
    \    Input Text    //div[@class='controlcontent']/parent::div//div/label[contains(text(),'Coordinates')]/following-sibling::div/input[2]    ${CoordinatesE}
    \    sleep    2s
    \    Input Text    //div[@class='controlcontent']/parent::div//div/label[contains(text(),'Area code')]/following-sibling::div/input    ${AreaCode}
    \    sleep    2s
    \    Input Text    //div[@class='controlcontent']/parent::div//div/label[contains(text(),'Phone number')]/following-sibling::div/input    ${PhoneNumber}
    \    sleep    2s
    \    Click Button    //div[@class='controlcontent']/div/button[@class=' btn btn-primary']
    \    log    'Verification of Availability check in Progress...'
    \    Comment    Wait Until Element Is Visible    //div[@class='clearfix requestinfo']//span[contains(text(),'Availability check in progress')]    240s
    \    Sleep    240s
    \    Reload Page
    \    log    1st reload
    \    sleep    60s
    \    Comment    Wait Until Element Is Visible    //div[@class='clearfix requestinfo']//span[contains(text(),'Availability check in progress')]    60s
    \    Reload Page
    \    log    2nd reload
    \    Comment    Wait Until Element Is Visible    //div[@class='clearfix requestinfo']//span[contains(text(),'Availability check in progress')]    60s
    \    Comment    sleep    \    240s
    \    Comment    Reload Page
    \    Comment    Wait Until Element Is Visible    //div[@class='clearfix requestinfo']//span[contains(text(),'Availability check in progress')]    180s
    \    Comment    Reload Page
    \    ${AccessDesign}    Get Text    xpath=//span[contains(.,'Access Design M')]//span[contains(.,'Available')]
    \    ${AccessDesignS}    Get Text    //span[contains(.,'Access Design S')]//span[contains(.,'Available')]
    \    ${AccessDesignXS}    Get Text    //span[contains(.,'Access Design XS')]//span[contains(.,'Available')]
    \    ${Logger1}=    Catenate    Access Design M is :    ${AccessDesign}
    \    ${Logger2}=    Catenate    Access Design S is :    ${AccessDesignS}
    \    ${Logger3}=    Catenate    Access Design is XS:    ${AccessDesignXS}
    \    Log    ${Logger1}
    \    Log    ${Logger2}
    \    Log    ${Logger3}

Customer Solution Form
    Set Browser Implicit Wait    30
    log    'User logged in and click on Menu'
    Click Element    //html/body/div[3]/div[1]/div/div/ul[1]/li/a/i[1]
    log    'Click on Enterprise Customers Menu option'
    Click Element    //html/body/div[5]/section/aside/ul/li[7]/a/span
    Page Should Contain Element    //html/body/div[5]/section/div/div/div[2]/div[1]/div
    Log    "In the Enterprice Customers Page"
    Click Element    //html/body/div[5]/section/div/div/div[2]/div[2]/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div/div[2]/div/table/tbody/tr/td[1]/div/a/span[1]
    Page Should Contain Element    //html/body/div[5]/section/div/div/div[2]/div[1]/div/div/div[4]/h1/span
    log    'New customer solution button click'
    Click Element    //html/body/div[5]/section/div/div/div[2]/div[2]/div/div/div/div/div/div/div/div/div/aside/div/div/div/div/div[4]/div/div/ul/li[3]/a/span
    log    'New customer solution titile check'
    Capture Page Screenshot
    Comment    Page Should Contain Element    .//*[@id='idcaca45']/div/h1/span
    Comment    sleep    30
    Select Frame    //iframe
    log    'wait for customer solution input box available'
    Wait Until Element Is Visible    //*[@id='_idJsp3_customForm_WAR_CustomForm_INSTANCE_5EkY_:_id1_customForm_WAR_CustomForm_INSTANCE_5EkY_']    5
    Capture Page Screenshot
    log    'enter customer solution name'
    Input Text    //*[@id='_idJsp3_customForm_WAR_CustomForm_INSTANCE_5EkY_:_id1_customForm_WAR_CustomForm_INSTANCE_5EkY_']    ${customerSolutionName}
    log    'click Add customer solution button'
    Comment    Click Element    //div[@class='buttons']//a
    Comment    Unselect Frame
    log    'Clicking on the Customer Solution Link'
    Sleep    5s
    Click Element    //div[@class='csrt_stream']//a//span[contains(text(),'${customerName}')]
    log    'Navigating to Customer Solution Name List page'
    log    Entering Customer solution name and clicking filter button
    Input Text    //div[@class='textFilter fulltext']//input    ${customerSolutionName}
    Click Element    //*[@class='filterValue']

Login_Form
    Open Browser    ${SiteUrl}    ${Browser}
    Set Selenium Speed    1
    log    'NGENA portal launched Successfully.'
    Page Should Contain Element    //*[@class='login_brand']
    Maximize Browser Window
    Input Text    //form[1]/input[3]    ${username}
    Input Text    //form/input[4]    ${password}
    Click Button    //form/div[2]/button
    Page Should Contain Element    //*[@class='menu_brand linksTo active']
    Page Should Contain Element    //div/span[contains(text(),'Customer service')]
    log    'Login successful.'
